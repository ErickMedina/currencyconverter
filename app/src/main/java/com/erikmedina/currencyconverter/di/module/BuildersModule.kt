package com.erikmedina.currencyconverter.di.module

import com.erikmedina.currencyconverter.presentation.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Binds all sub-components within the app.
 */
@Module
abstract class BuildersModule {

    @ContributesAndroidInjector(modules = [MainModule::class, NetworkModule::class])
    internal abstract fun bindMainActivity(): MainActivity

    // Add bindings for other sub-components here
}
