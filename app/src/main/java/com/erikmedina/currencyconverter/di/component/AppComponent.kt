package com.erikmedina.currencyconverter.di.component

import com.erikmedina.currencyconverter.App
import com.erikmedina.currencyconverter.di.module.AppModule
import com.erikmedina.currencyconverter.di.module.BuildersModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, BuildersModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
