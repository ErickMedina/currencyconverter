package com.erikmedina.currencyconverter.di.module

import com.erikmedina.currencyconverter.domain.repository.Repository
import com.erikmedina.currencyconverter.domain.repository.RepositoryImpl
import com.erikmedina.currencyconverter.domain.usecase.GetLatestRates
import com.erikmedina.currencyconverter.domain.usecase.GetLatestRatesImpl
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    fun provideGetLatestRates(getLatestRatesImpl: GetLatestRatesImpl): GetLatestRates {
        return getLatestRatesImpl
    }

    @Provides
    fun provideRepository(repositoryImpl: RepositoryImpl): Repository {
        return repositoryImpl
    }
}
