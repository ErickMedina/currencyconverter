package com.erikmedina.currencyconverter.di.module

import android.content.Context
import com.erikmedina.currencyconverter.App
import com.erikmedina.currencyconverter.framework.network.ApiRest
import com.erikmedina.currencyconverter.framework.network.RetrofitFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    internal fun provideContext(application: App): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    internal fun provideApiRest(): ApiRest {
        return RetrofitFactory.createWebService()
    }
}
