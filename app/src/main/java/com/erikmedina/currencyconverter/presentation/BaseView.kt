package com.erikmedina.currencyconverter.presentation

interface BaseView<T> {

    fun showError(error: String)

    fun showLoading(show: Boolean)
}
