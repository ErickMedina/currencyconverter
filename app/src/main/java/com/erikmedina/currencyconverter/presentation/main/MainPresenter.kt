package com.erikmedina.currencyconverter.presentation.main

import android.os.Handler
import android.util.Log
import com.erikmedina.currencyconverter.data.model.Rate
import com.erikmedina.currencyconverter.domain.usecase.GetLatestRates
import java.util.*
import javax.inject.Inject

class MainPresenter
@Inject
constructor(private val getLatestRates: GetLatestRates) : MainContract.Presenter {

    private var view: MainContract.View? = null
    private var timer: Timer? = null
    private lateinit var previousRate: String
    private lateinit var currentCurrencyCode: String

    override fun takeView(view: MainContract.View) {
        this.view = view
    }

    override fun dropView() {
        this.view = null
    }

    override fun updateCurrentCurrencyCode(currencyCode: String) {
        currentCurrencyCode = currencyCode
    }

    override fun getRatesForFirstTime() {
        view?.showLoading(true)
        previousRate = currentCurrencyCode
        getLatestRates.run(currentCurrencyCode, object : GetLatestRates.Callback {
            override fun onSuccess(rateList: ArrayList<Rate>) {
                view?.showLoading(false)
                view?.setItemViews(rateList)
                startTimer()
            }

            override fun onError(throwable: Throwable) {
                view?.showLoading(false)
                view?.showError("There was a problem. Try later")
            }
        })
    }

    override fun startTimer() {
        if (timer == null) {
            Log.i(TAG, "Timer started...")
            val handler = Handler()
            timer = Timer()
            val doAsynchronousTask = object : TimerTask() {
                override fun run() {
                    handler.post {
                        updateRates()
                    }
                }
            }
            timer!!.schedule(doAsynchronousTask, 0L, 5000L)
        }
    }

    private fun updateRates() {
        view?.showLoading(true)
        getLatestRates.run(currentCurrencyCode, object : GetLatestRates.Callback {
            override fun onSuccess(rateList: ArrayList<Rate>) {
                view?.showLoading(false)
                if (previousRate == currentCurrencyCode) {
                    view?.updateRates()
                } else {
                    previousRate = currentCurrencyCode
                    view?.updateRates()
                    stopTimer()
                    view?.reorganizeRates()
                }
            }

            override fun onError(throwable: Throwable) {
                view?.showLoading(false)
                view?.showError("There was a problem. Try later")
            }
        })
    }

    override fun stopTimer() {
        Log.i(TAG, "Timer stopped...")
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    companion object {
        private val TAG = MainPresenter::class.java.simpleName
    }
}
