package com.erikmedina.currencyconverter.presentation.main.adapter

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.erikmedina.currencyconverter.R
import com.erikmedina.currencyconverter.data.model.Rate
import com.erikmedina.currencyconverter.helper.CalculationHelper

class RateAdapter(
    private val listener: OnItemClickListener,
    private val calculationHelper: CalculationHelper
) : RecyclerView.Adapter<RateAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onRateClicked(position: Int, rate: Rate)
    }

    private var itemViews = mutableListOf<Rate>()
    private var onBind: Boolean = false
    private var baseAmount: Double = 1.0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_rate, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = itemViews.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        updateRateAmount(position)
        holder.setFieldValues(position)
        holder.bind(position, itemViews[position], listener)
    }

    private fun updateRateAmount(position: Int) {
        if (position == 0) {
            itemViews[position].amount = baseAmount
        } else {
            itemViews[position].amount =
                    calculationHelper.calculateAmount(baseAmount, itemViews[position].rate)
        }
    }


    fun setItemViews(items: List<Rate>) {
        this.itemViews = items as MutableList<Rate>
        notifyDataSetChanged()
    }

    fun updateRates() {
        notifyItemRangeChanged(1, itemViews.size - 1)
    }

    fun reorganizeRows(rate: Rate) {
        itemViews.remove(rate)
        itemViews.add(0, rate)
        baseAmount = rate.amount
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvCurrencyCode: TextView = itemView.findViewById(R.id.tvCurrencyCode)
        var tvCurrency: TextView = itemView.findViewById(R.id.tvCurrency)
        var etAmount: EditText = itemView.findViewById(R.id.etAmount)

        fun setFieldValues(position: Int) {
            tvCurrencyCode.text = itemViews[position].currencyCode
            onBind = true
            etAmount.setText(itemViews[position].amount.toString(), TextView.BufferType.EDITABLE)
            onBind = false
        }

        fun bind(position: Int, rate: Rate, listener: OnItemClickListener) {
            etAmount.setOnFocusChangeListener { view, hasFocus ->
                if (hasFocus) {
                    listener.onRateClicked(position, rate)
                }
            }
            etAmount.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (!onBind) {
                        baseAmount = if (s.toString().isEmpty()) {
                            0.0
                        } else {
                            s.toString().toDouble()
                        }
                        notifyItemRangeChanged(1, itemViews.size - 1)
                    }
                }
            })
        }
    }
}
