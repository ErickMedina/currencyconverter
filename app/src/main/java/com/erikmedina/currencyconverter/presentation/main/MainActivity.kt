package com.erikmedina.currencyconverter.presentation.main

import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.erikmedina.currencyconverter.R
import com.erikmedina.currencyconverter.data.model.Rate
import com.erikmedina.currencyconverter.helper.CalculationHelper
import com.erikmedina.currencyconverter.presentation.main.adapter.RateAdapter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainPresenter
    @Inject
    lateinit var calculationHelper: CalculationHelper

    private lateinit var rateAdapter: RateAdapter
    private lateinit var currentRate: Rate
    private var currentCurrencyCode = "EUR"

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "[onCreate]...")
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeRecycler()
    }

    private fun retrieveCurrencyCodeIfExists() {//We store the currency code so if the app goes to background and comes back to foreground, the data are consistent
        currentCurrencyCode =
                PreferenceManager.getDefaultSharedPreferences(this).getString(TAG_PREFERENCES, "EUR")
    }

    private fun initializeRecycler() {
        val linearLayoutManager = LinearLayoutManager(this)
        recycler.layoutManager = linearLayoutManager
        recycler.smoothScrollToPosition(0)
        rateAdapter = RateAdapter(object : RateAdapter.OnItemClickListener {
            override fun onRateClicked(position: Int, rate: Rate) {
                Log.i(TAG, "[onRateClicked] rate selected: ${rate.currencyCode}")
                if (position != 0) {
                    currentRate = rate
                    presenter.stopTimer()
                    storeCurrencyCode(rate.currencyCode)
                    presenter.updateCurrentCurrencyCode(rate.currencyCode)
                    presenter.startTimer()
                }
            }
        }, calculationHelper)
        recycler.adapter = rateAdapter
    }

    private fun storeCurrencyCode(currencyCode: String) {
        PreferenceManager.getDefaultSharedPreferences(applicationContext).edit()
            .putString(TAG_PREFERENCES, currencyCode).apply()
    }

    override fun onResume() {
        super.onResume()
        presenter.takeView(this)
        retrieveCurrencyCodeIfExists()
        presenter.updateCurrentCurrencyCode(currentCurrencyCode)
        presenter.getRatesForFirstTime()
    }

    override fun onPause() {
        super.onPause()
        presenter.stopTimer()
        presenter.dropView()
    }

    override fun setItemViews(rateList: List<Rate>) {
        rateAdapter.setItemViews(rateList)
    }

    override fun updateRates() {
        rateAdapter.updateRates()
    }

    override fun reorganizeRates() {
        rateAdapter.reorganizeRows(currentRate)
        recycler.layoutManager?.scrollToPosition(0)
        presenter.startTimer()
    }

    override fun showError(error: String) {
        val rootView = window.decorView.findViewById<View>(android.R.id.content)
        Snackbar.make(rootView, error, Snackbar.LENGTH_SHORT).show()
    }

    override fun showLoading(show: Boolean) {
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }


    companion object {
        val TAG = MainActivity::class.java.simpleName.toString()
        const val TAG_PREFERENCES = "com.erikmedina.preferences"
    }
}
