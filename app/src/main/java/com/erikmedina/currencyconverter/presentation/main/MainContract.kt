package com.erikmedina.currencyconverter.presentation.main

import com.erikmedina.currencyconverter.data.model.Rate
import com.erikmedina.currencyconverter.presentation.BasePresenter
import com.erikmedina.currencyconverter.presentation.BaseView

interface MainContract {

    interface View : BaseView<Presenter> {

        fun setItemViews(rateList: List<Rate>)

        fun updateRates()

        fun reorganizeRates()
    }

    interface Presenter : BasePresenter<View> {

        fun updateCurrentCurrencyCode(currencyCode: String)

        fun getRatesForFirstTime()

        fun startTimer()

        fun stopTimer()
    }
}
