package com.erikmedina.currencyconverter.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Rates {

    @SerializedName("EUR")
    @Expose
    var eur: Double = 1.0
    @SerializedName("AUD")
    @Expose
    var aud: Double = 1.0
    @SerializedName("BGN")
    @Expose
    var bgn: Double = 1.0
    @SerializedName("BRL")
    @Expose
    var brl: Double = 1.0
    @SerializedName("CAD")
    @Expose
    var cad: Double = 1.0
    @SerializedName("CHF")
    @Expose
    var chf: Double = 1.0
    @SerializedName("CNY")
    @Expose
    var cny: Double = 1.0
    @SerializedName("CZK")
    @Expose
    var czk: Double = 1.0
    @SerializedName("DKK")
    @Expose
    var dkk: Double = 1.0
    @SerializedName("GBP")
    @Expose
    var gbp: Double = 1.0
    @SerializedName("HKD")
    @Expose
    var hkd: Double = 1.0
    @SerializedName("HRK")
    @Expose
    var hrk: Double = 1.0
    @SerializedName("HUF")
    @Expose
    var huf: Double = 1.0
    @SerializedName("IDR")
    @Expose
    var idr: Double = 1.0
    @SerializedName("ILS")
    @Expose
    var ils: Double = 1.0
    @SerializedName("INR")
    @Expose
    var inr: Double = 1.0
    @SerializedName("ISK")
    @Expose
    var isk: Double = 1.0
    @SerializedName("JPY")
    @Expose
    var jpy: Double = 1.0
    @SerializedName("KRW")
    @Expose
    var krw: Double = 1.0
    @SerializedName("MXN")
    @Expose
    var mxn: Double = 1.0
    @SerializedName("MYR")
    @Expose
    var myr: Double = 1.0
    @SerializedName("NOK")
    @Expose
    var nok: Double = 1.0
    @SerializedName("NZD")
    @Expose
    var nzd: Double = 1.0
    @SerializedName("PHP")
    @Expose
    var php: Double = 1.0
    @SerializedName("PLN")
    @Expose
    var pln: Double = 1.0
    @SerializedName("RON")
    @Expose
    var ron: Double = 1.0
    @SerializedName("RUB")
    @Expose
    var rub: Double = 1.0
    @SerializedName("SEK")
    @Expose
    var sek: Double = 1.0
    @SerializedName("SGD")
    @Expose
    var sgd: Double = 1.0
    @SerializedName("THB")
    @Expose
    var thb: Double = 1.0
    @SerializedName("TRY")
    @Expose
    var trai: Double = 1.0
    @SerializedName("USD")
    @Expose
    var usd: Double = 1.0
    @SerializedName("ZAR")
    @Expose
    var zar: Double = 1.0
}
