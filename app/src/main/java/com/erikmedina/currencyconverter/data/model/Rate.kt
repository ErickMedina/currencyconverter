package com.erikmedina.currencyconverter.data.model

data class Rate(
    var currencyCode: String = "N/A",
    var rate: Double = 0.0,
    var amount: Double = 0.0
)
