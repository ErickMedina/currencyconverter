package com.erikmedina.currencyconverter.data.mapper

import com.erikmedina.currencyconverter.data.model.LatestRates
import com.erikmedina.currencyconverter.data.model.Rate

object Mapper {

    private var rateList: ArrayList<Rate>? = null

    private lateinit var rateEur: Rate
    private lateinit var rateAud: Rate
    private lateinit var rateBgn: Rate
    private lateinit var rateBrl: Rate
    private lateinit var rateCad: Rate
    private lateinit var rateChf: Rate
    private lateinit var rateCny: Rate
    private lateinit var rateCzk: Rate
    private lateinit var rateDkk: Rate
    private lateinit var rateGbp: Rate
    private lateinit var rateHkd: Rate
    private lateinit var rateHrk: Rate
    private lateinit var rateHuf: Rate
    private lateinit var rateIdr: Rate
    private lateinit var rateIls: Rate
    private lateinit var rateInr: Rate
    private lateinit var rateIsk: Rate
    private lateinit var rateJpy: Rate
    private lateinit var rateKrw: Rate
    private lateinit var rateMxn: Rate
    private lateinit var rateMyr: Rate
    private lateinit var rateNok: Rate
    private lateinit var rateNzd: Rate
    private lateinit var ratePhp: Rate
    private lateinit var ratePln: Rate
    private lateinit var rateRon: Rate
    private lateinit var rateRub: Rate
    private lateinit var rateSek: Rate
    private lateinit var rateSgd: Rate
    private lateinit var rateThb: Rate
    private lateinit var rateTry: Rate
    private lateinit var rateUsd: Rate
    private lateinit var rateZar: Rate

    fun mapToRateList(latestRates: LatestRates): ArrayList<Rate> {
        if (rateList == null) {
            rateList = ArrayList()
            val rates = latestRates.rates
            rates?.run {
                rateEur = Rate()
                rateEur.currencyCode = "EUR"
                rateEur.rate = rates.eur
                rateList!!.add(rateEur)
                rateAud = Rate()
                rateAud.currencyCode = "AUD"
                rateAud.rate = rates.aud
                rateList!!.add(rateAud)
                rateBgn = Rate()
                rateBgn.currencyCode = "BGN"
                rateBgn.rate = rates.bgn
                rateList!!.add(rateBgn)
                rateBrl = Rate()
                rateBrl.currencyCode = "BRL"
                rateBrl.rate = rates.brl
                rateList!!.add(rateBrl)
                rateCad = Rate()
                rateCad.currencyCode = "CAD"
                rateCad.rate = rates.cad
                rateList!!.add(rateCad)
                rateChf = Rate()
                rateChf.currencyCode = "CHF"
                rateChf.rate = rates.chf
                rateList!!.add(rateChf)
                rateCny = Rate()
                rateCny.currencyCode = "CNY"
                rateCny.rate = rates.cny
                rateList!!.add(rateCny)
                rateCzk = Rate()
                rateCzk.currencyCode = "CZK"
                rateCzk.rate = rates.czk
                rateList!!.add(rateCzk)
                rateDkk = Rate()
                rateDkk.currencyCode = "DKK"
                rateDkk.rate = rates.dkk
                rateList!!.add(rateDkk)
                rateGbp = Rate()
                rateGbp.currencyCode = "GBP"
                rateGbp.rate = rates.gbp
                rateList!!.add(rateGbp)
                rateHkd = Rate()
                rateHkd.currencyCode = "HKD"
                rateHkd.rate = rates.hkd
                rateList!!.add(rateHkd)
                rateHrk = Rate()
                rateHrk.currencyCode = "HRK"
                rateHrk.rate = rates.hrk
                rateList!!.add(rateHrk)
                rateHuf = Rate()
                rateHuf.currencyCode = "HUF"
                rateHuf.rate = rates.huf
                rateList!!.add(rateHuf)
                rateIdr = Rate()
                rateIdr.currencyCode = "IDR"
                rateIdr.rate = rates.idr
                rateList!!.add(rateIdr)
                rateIls = Rate()
                rateIls.currencyCode = "ILS"
                rateIls.rate = rates.ils
                rateList!!.add(rateIls)
                rateInr = Rate()
                rateInr.currencyCode = "INR"
                rateInr.rate = rates.inr
                rateList!!.add(rateInr)
                rateIsk = Rate()
                rateIsk.currencyCode = "ISK"
                rateIsk.rate = rates.isk
                rateList!!.add(rateIsk)
                rateJpy = Rate()
                rateJpy.currencyCode = "JPY"
                rateJpy.rate = rates.jpy
                rateList!!.add(rateJpy)
                rateKrw = Rate()
                rateKrw.currencyCode = "KRW"
                rateKrw.rate = rates.krw
                rateList!!.add(rateKrw)
                rateMxn = Rate()
                rateMxn.currencyCode = "MXN"
                rateMxn.rate = rates.mxn
                rateList!!.add(rateMxn)
                rateMyr = Rate()
                rateMyr.currencyCode = "MYR"
                rateMyr.rate = rates.myr
                rateList!!.add(rateMyr)
                rateNok = Rate()
                rateNok.currencyCode = "NOK"
                rateNok.rate = rates.nok
                rateList!!.add(rateNok)
                rateNzd = Rate()
                rateNzd.currencyCode = "NZD"
                rateNzd.rate = rates.nzd
                rateList!!.add(rateNzd)
                ratePhp = Rate()
                ratePhp.currencyCode = "PHP"
                ratePhp.rate = rates.php
                rateList!!.add(ratePhp)
                ratePln = Rate()
                ratePln.currencyCode = "PLN"
                ratePln.rate = rates.pln
                rateList!!.add(ratePln)
                rateRon = Rate()
                rateRon.currencyCode = "RON"
                rateRon.rate = rates.ron
                rateList!!.add(rateRon)
                rateRub = Rate()
                rateRub.currencyCode = "RUB"
                rateRub.rate = rates.rub
                rateList!!.add(rateRub)
                rateSek = Rate()
                rateSek.currencyCode = "SEK"
                rateSek.rate = rates.sek
                rateList!!.add(rateSek)
                rateSgd = Rate()
                rateSgd.currencyCode = "SGD"
                rateSgd.rate = rates.sgd
                rateList!!.add(rateSgd)
                rateThb = Rate()
                rateThb.currencyCode = "THB"
                rateThb.rate = rates.thb
                rateList!!.add(rateThb)
                rateTry = Rate()
                rateTry.currencyCode = "TRY"
                rateTry.rate = rates.trai
                rateList!!.add(rateTry)
                rateUsd = Rate()
                rateUsd.currencyCode = "USD"
                rateUsd.rate = rates.usd
                rateList!!.add(rateUsd)
                rateZar = Rate()
                rateZar.currencyCode = "ZAR"
                rateZar.rate = rates.zar
                rateList!!.add(rateZar)
            }
        } else {
            updateToRateList(latestRates)
        }
        return rateList as ArrayList<Rate>
    }

    private fun updateToRateList(latestRates: LatestRates) {
        val rates = latestRates.rates
        rates?.run {
            rateEur.rate = rates.eur
            rateAud.rate = rates.aud
            rateBgn.rate = rates.bgn
            rateBrl.rate = rates.brl
            rateCad.rate = rates.cad
            rateChf.rate = rates.chf
            rateCny.rate = rates.cny
            rateCzk.rate = rates.czk
            rateDkk.rate = rates.dkk
            rateGbp.rate = rates.gbp
            rateHkd.rate = rates.hkd
            rateHrk.rate = rates.hrk
            rateHuf.rate = rates.huf
            rateIdr.rate = rates.idr
            rateIls.rate = rates.ils
            rateInr.rate = rates.inr
            rateIsk.rate = rates.isk
            rateJpy.rate = rates.jpy
            rateKrw.rate = rates.krw
            rateMxn.rate = rates.mxn
            rateMyr.rate = rates.myr
            rateNok.rate = rates.nok
            rateNzd.rate = rates.nzd
            ratePhp.rate = rates.php
            ratePln.rate = rates.pln
            rateRon.rate = rates.ron
            rateRub.rate = rates.rub
            rateSek.rate = rates.sek
            rateSgd.rate = rates.sgd
            rateThb.rate = rates.thb
            rateTry.rate = rates.trai
            rateUsd.rate = rates.usd
            rateZar.rate = rates.zar
        }
    }
}
