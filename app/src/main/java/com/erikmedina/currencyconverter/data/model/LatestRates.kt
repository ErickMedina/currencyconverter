package com.erikmedina.currencyconverter.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LatestRates {

    @SerializedName("base")
    @Expose
    var base: String? = null
    @SerializedName("date")
    @Expose
    var date: String? = null
    @SerializedName("rates")
    @Expose
    var rates: Rates? = null
}
