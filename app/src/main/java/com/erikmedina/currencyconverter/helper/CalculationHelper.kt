package com.erikmedina.currencyconverter.helper

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CalculationHelper
@Inject constructor() {

    fun calculateAmount(baseAmount: Double, rate: Double) = baseAmount * rate
}
