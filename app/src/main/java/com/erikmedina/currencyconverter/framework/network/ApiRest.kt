package com.erikmedina.currencyconverter.framework.network

import com.erikmedina.currencyconverter.data.model.LatestRates
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The API for all the requests of the application
 */
interface ApiRest {

    @GET("latest")
    fun getLatestRates(@Query("base") base: String): Call<LatestRates>
}
