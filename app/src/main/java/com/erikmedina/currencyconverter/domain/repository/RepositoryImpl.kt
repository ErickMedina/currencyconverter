package com.erikmedina.currencyconverter.domain.repository

import android.util.Log
import com.erikmedina.currencyconverter.data.mapper.Mapper
import com.erikmedina.currencyconverter.data.model.LatestRates
import com.erikmedina.currencyconverter.data.model.Rate
import com.erikmedina.currencyconverter.framework.network.ApiRest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * The repository will be responsible for transforming the data for the input and output of information.
 */
@Singleton
class RepositoryImpl
@Inject
constructor(private val apiRest: ApiRest) : Repository {//We use DI constructor because this class is our property

    override fun getRates(base: String, callback: Repository.Callback<ArrayList<Rate>>) {
        Log.i(TAG, "[updateRates]")
        val call = apiRest.getLatestRates(base)
        call.enqueue(object : Callback<LatestRates> {
            override fun onResponse(call: Call<LatestRates>?, response: Response<LatestRates>) {
                if (response.isSuccessful) {
                        callback.onSuccess(Mapper.mapToRateList(response.body()))
                }
            }

            override fun onFailure(call: Call<LatestRates>?, t: Throwable) {
                callback.onError(t)
            }
        })
    }

    companion object {
        val TAG = RepositoryImpl::class.java.simpleName.toString()
    }
}
