package com.erikmedina.currencyconverter.domain.usecase

import com.erikmedina.currencyconverter.data.model.Rate

interface GetLatestRates : UseCase {

    interface Callback {

        fun onSuccess(rateList: ArrayList<Rate>)

        fun onError(throwable: Throwable)
    }

    fun run(base: String, callback: Callback)
}
