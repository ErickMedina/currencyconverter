package com.erikmedina.currencyconverter.domain.repository

import com.erikmedina.currencyconverter.data.model.Rate

interface Repository {

    interface Callback<T> {

        fun onSuccess(generic: T)

        fun onError(throwable: Throwable)
    }

    fun getRates(base: String, callback: Callback<ArrayList<Rate>>)
}
