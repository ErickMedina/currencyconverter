package com.erikmedina.currencyconverter.domain.usecase

import com.erikmedina.currencyconverter.data.model.Rate
import com.erikmedina.currencyconverter.domain.repository.Repository
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import javax.inject.Inject

class GetLatestRatesImpl @Inject constructor(private val repository: Repository) : GetLatestRates {

    private lateinit var base: String
    private lateinit var callback: GetLatestRates.Callback

    override fun run(base: String, callback: GetLatestRates.Callback) {
        this.base = base
        this.callback = callback
        execute()
    }

    override fun execute() {
        launch(UI) {
            repository.getRates(base, object : Repository.Callback<ArrayList<Rate>> {
                override fun onSuccess(rateList: ArrayList<Rate>) {
                    callback.onSuccess(rateList)
                }

                override fun onError(throwable: Throwable) {
                    callback.onError(throwable)
                }
            })
        }
    }
}
